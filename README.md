# Options Parser Trait
I'm not sure if this is terribly useful, but I find reading `$this->opt->use_blue` easier
to read then having the bitwise operators inline when I need to check

# Example Usage

	class CoffeeMaker
	{
		use OptionsTrait;

		const OPT_USE_WHIPCREAM = 1;
		const OPT_USE_MILK      = 2;
		const OPT_USE_SUGAR     = 4;
		const OPT_USE_CREAMER   = 8;
		const OPT_USE_X         = 16;
		const OPT_USE_ALL       = 31;

		public function __construct( $opts )
		{
			$this->_parse_options( $opts );
		}

		public function should_we_use_sugar()
		{
			echo $this->opts->use_sugar ? 'yes' : 'no';  
		}

	}

	$a = new CoffeeMaker( CoffeeMaker::OPT_USE_ALL ^ CoffeeMaker::OPT_USE_SUGAR );
	var_dump( $a );

# Naming Options
For this to work, option contants MUST be named OPT_OPTION_NAME - the prefix of `OPT_`
is required.

# Parsing Options
The options value passed to the contructor should be then passed to _parse_options as
in the above example

# Using
Just like any other bitwise options ... for example:

	# means true for everything but use_sugar
	CoffeeMaker::OPT_USE_ALL ^ CoffeeMaker::OPT_USE_SUGAR

	# sets use_milk and use_creamer to true everything else false
	CoffeeMaker::OPT_USE_MILK & CoffMaker::OPT_USE_CREAMER

In the code that uses the options ...

	# now you can use this
	$this->opts->use_milk && $this->addMilk();

