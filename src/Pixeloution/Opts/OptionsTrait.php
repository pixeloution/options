<?php 
# ----------------------------------------------------------------------------------------
namespace Pixeloution\Opts;
# ----------------------------------------------------------------------------------------

trait OptionsTrait
{
   /**
    * used to store the parsed options, ie. $this->opts->use_milk
    * @var bool
    */
   public $opts;

   /**
    * should be called from the object's contructor with the options val
    * @param  int $options
    * options value
    * 
    * @return void
    */
   protected function _parse_options( $options = null )
   {
      $this->_build_options_list((new \ReflectionClass( __CLASS__ ))->getConstants(), $options);
   }

   /**
    * runs through each constant beginning with OPT_ and parses them into 
    * $this->opts->opt_name
    * 
    * @param  array $constants
    * @param  int   $options
    * 
    * @return void
    */
   protected function _build_options_list( $constants, $options )
   {
      $this->opts = new \StdClass();
      
      foreach( $constants as $key => $value )
      {
         if( preg_match('/^OPT_(.*?)$/', $key, $matches) )
         {
            $property = strtolower( $matches[1] );
            $this->opts->$property = $value & $options ? true : false;
         }
      }
   }
}
